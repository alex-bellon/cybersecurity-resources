# Reconnaisance

## Certificates

### Certificate search

[crt.sh](https://crt.sh/)
[Google certificate search](https://transparencyreport.google.com/https/certificates?hl=en)
[Entrust certifcate search](https://www.entrust.com/ct-search/)

## OSINT

[Intel Techniques](https://inteltechniques.com/links.html)

![https://i.redd.it/370mx0gln0k01.jpg](https://i.redd.it/370mx0gln0k01.jpg)
