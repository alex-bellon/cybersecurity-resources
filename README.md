# Cybersecurity Resources

## Articles

[Pentesting with an Android phone](https://www.peerlyst.com/posts/pentesting-with-an-android-phone-part-1-preparing-the-phone-david-dunmore?trk=profile_page_overview_panel_posts)

## Blogs

[Google Project Zero](https://googleprojectzero.blogspot.com/) - Zero day research from Google

[Phrack](https://phrack.org)

## Books

https://please.dont-hack.me/books/hacking/

https://doc.lagout.org/

## Certificate

[Network+ Study Guide](https://docs.google.com/document/d/1iyFK7tvM5JVhRnNH6IWnZ07OGlHcxmEEVVv56Rccsng/edit)

[OSCP](https://www.offensive-security.com/information-security-certifications/oscp-offensive-security-certified-professional/)

[Security+ Study Guide](https://docs.google.com/document/d/1hXGyKDWdpJLKZWWuu5eVTh-N5simhpSlTTA-Z-dtCj0/edit)

## Cheatsheets

[Must have cheatsheet, r/AskNetSec](https://www.reddit.com/r/AskNetsec/comments/98jqgb/what_are_some_must_have_cheatsheets/)

[Netcat Cheat Sheet](https://www.sans.org/security-resources/sec560/netcat_cheat_sheet_v1.pdf)

[PenTesting tools cheatsheet](https://highon.coffee/blog/penetration-testing-tools-cheat-sheet/)

[Black Alps](https://www.blackalps.ch/ba-18/)

## Learning

http://www.securitytube.net/

http://cybrary.it//

http://null-byte.wonderhowto.com/how-to/

http://n0where.net/

http://www.offensive-security.com/metasploit-unleashed

http://www.exploit-db.com/

https://siph0n.net/

http://www.cvedetails.com/

http://resources.infosecinstitute.com/

http://www.windowsecurity.com/articles-tutorials/

http://www.securitysift.com/

http://www.sans.org/reading-room/

http://packetstormsecurity.com/files/

https://www.corelan.be/index.php/articles/

http://routerpwn.com/

http://opensecuritytraining.info/Training.html

https://www.blackhat.com/html/archives.html

http://magazine.hitb.org/hitb-magazine.html

https://www.hacksplaining.com/lessons

http://securityidiots.com

https://blog.skullsecurity.org/

## MOOC

https://cybersecuritybase.mooc.fi/

## News

https://threatpost.com/

http://www.deepdotweb.com/

https://packetstormsecurity.com/

http://www.cvedetails.com/

## Papers

[James Mickens](https://mickens.seas.harvard.edu/wisdom-james-mickens) - Harvard cybersecurity professor

## Podcasts

[10 Cybersecurity Podcasts, r/cybersecurity](https://www.reddit.com/r/cybersecurity/comments/8qqyw2/10_cybersecurity_podcasts_you_should_listen_to_in/)

## Practice

[Hack the Box](https://www.hackthebox.eu/)

[r/AskNetSec](https://www.reddit.com/r/AskNetsec/comments/8brzbr/getting_into_pen_testinghow_do_i_dive_into_ctf)

[23 Practice sites, r/HowToHack](https://www.reddit.com/r/HowToHack/comments/81f3hl/23_hacking_sites_ctfs_and_wargames_to_legally)

http://overthewire.org/wargames/

https://www.pentesterlab.com/

http://www.itsecgames.com/

https://exploit-exercises.com/

http://www.enigmagroup.org/

http://smashthestack.org/

http://3564020356.org/

http://www.hackthissite.org/

http://www.hackertest.net/

http://0x0539.net/

http://hacking.voyage/

https://ctf.hacker101.com/about

https://attackdefense.com/

[List of Practice CTF sites](http://captf.com/practice-ctf/)

[Black Box Society - The Box](https://blackboxsociety.org/the-box/) - Always on CTF

## Privacy

[PrivacyTools.io](https://www.privacytools.io/)

[That One Privacy Site](https://thatoneprivacysite.net/)

## Projects

[GitHub Showcase](https://github.com/topics/security)

[Security Programming Projects, r/AskNetSec](https://www.reddit.com/r/AskNetsec/comments/8chw3s/what_are_good_cyber_security_programming_projects/)

## Reddit

### Posts

[Videos and courses](https://www.reddit.com/r/Hacking_Tutorials/comments/aczz0l/if_you_like_watching_tutorials_to_learn_dump_of/)

[Learn NetSec](https://www.reddit.com/r/AskNetsec/comments/ar3fo5/learn_netsec/)

### Subreddits

[r/Cybersecurity](https://www.reddit.com/r/cybersecurity)

[r/HowToHack](https://www.reddit.com/r/howtohack)

[r/NetSec](https://www.reddit.com/r/netsec)

[r/NetSecStudents](https://www.reddit.com/r/netsecstudents)

[r/Security](https://www.reddit.com/r/security)

[r/SecurityCTF](https://www.reddit.com/r/securityctf)

## Software

### Distributions

https://www.kali.org/

http://sourceforge.net/projects/metasploitable/

https://tails.boum.org/

http://ophcrack.sourceforge.net/

https://n0where.net/blackarch-linux/

http://seanux.net/index/en

http://sourceforge.net/p/attackvector/wiki/Home/(edited)

## Talks

[BugBounty Talk](https://docs.google.com/presentation/d/1xgvEScGZ_ukNY0rmfKz1JN0sn-CgZY_rTp2B_SZvijk/mobilepresent#slide=id.g4052c4692d_0_0)

## Websites

[Really nice website for figureing out where to start on CTF problems](https://ctf101.org/)

[cybrary.it](https://www.cybrary.it/)

[OSCP Goldmine](http://0xc0ffee.io/blog/OSCP-Goldmine )

[ashot.org](https://www.ashot.org/links.php)

[Vulnerable sites, software and wargames](https://www.amanhardikar.com/mindmaps/Practice.html)

https://cybr.club/Important-Links/

https://www.hacksplaining.com/

https://www.exploit-db.com/google-hacking-database - Making it easier to use Google queries for pentesting

[Beginner's Curriculum](https://s3ctur.wordpress.com/2017/06/19/breaking-into-infosec-a-beginners-curriculum/)

[CTF Practice](https://zaratec.github.io/references/)

[InfoSecIITR Reading Material](https://github.com/infoseciitr/reading-material)

[CTF and Pentesting Tools](https://prune2000.github.io/tools/pentest/)

## Writeups

### Writeups that I like, or explained problems I didn't solve, or taught me something new

[Google CTF 2018 - Rubik](http://blog.ebfe.dk/ctf/2017/06/20/googlectf2017-rubik/)

[TSG CTF 2019 - Obliterated File 1-2 Writeup](https://0xsaiyajin.github.io/writeup/2019/05/06/tsgctf-obliterated-file-1-2-writeup-eng.html)

[UTSA Cyber CTF 2019 - Linux2](https://0xsaiyajin.github.io/writeup/2019/05/02/utsacyber-ctf-linux-2-writeup-eng.html)

[UUCTF 2019](https://medium.com/bugbountywriteup/uutcf-2019-writeups-6c11ea3f9d22)

## YouTube Channels

[13Cubed](https://www.youtube.com/user/davisrichardg/videos)

[/dev/null](https://www.youtube.com/channel/UCGISJ8ZHkmIv1CaoHovK-Xw/videos)

[Gynvael Coldwind](https://www.youtube.com/user/GynvaelEN/videos)

[IppSec](https://www.youtube.com/channel/UCa6eh7gCkpPo5XXUDfygQQA/videos)

[LiveOverflow](https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w/videos)
