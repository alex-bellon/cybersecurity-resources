# Crypto

# Cryptography

## Books

[Applied Cryptography](https://www.schneier.com/books/applied_cryptography/)

[Cracking codes with Python](https://nostarch.com/crackingcodes)

[Cryptography Engineering](https://www.schneier.com/books/cryptography_engineering/)

[Serious Cryptography](https://nostarch.com/seriouscrypto)

[The Code Book](https://en.wikipedia.org/wiki/The_Code_Book)

[The Codebreakers](https://en.wikipedia.org/wiki/The_Codebreakers)

## Competitions

[International Olympiad in Cryptography](https://nsucrypto.nsu.ru/)

## Concepts

[A Crash Course in Everything Cryptographic](https://medium.com/@lduck11007/a-crash-course-in-everything-cryptographic-50daa0fda482)

[A Guide to Post-Quantum Cryptography](https://hackernoon.com/a-guide-to-post-quantum-cryptography-d785a70ea04b)

[Cryptographic Attacks](https://en.wikipedia.org/wiki/Category:Cryptographic_attacks)

[Shor's Algorithm](https://www.scottaaronson.com/blog/?p=208)

### Ciphers

[Beaufort Cipher](http://practicalcryptography.com/ciphers/beaufort-cipher/)

[Pollux Cipher](https://www.dcode.fr/pollux-cipher)

[VIC Cipher](https://en.wikipedia.org/wiki/VIC_cipher)

### Encryptions

El Gamal Encryption - Public Key Cryptosystem ([Wikipedia](https://en.wikipedia.org/wiki/ElGamal_encryption#frb-inline))

## General

[A Graduate Course in Applied Cryptography](https://toc.cryptobook.us/) - Full textbook online

[Dan Boneh](https://crypto.stanford.edu/~dabo/) - Crypto prof at Stanford, has a lot of good resources (including a textbook!)

[The Amazing King](http://theamazingking.com/crypto.php)

[An Introduction to Mathematical Cryptography](https://www.math.brown.edu/~jhs/MathCryptoHome.html)

## Practice

[Cryptopals](https://cryptopals.com/)

[ToadStyle Cryptopals](https://toadstyle.org/cryptopals/)

## Tools

[hashID](https://github.com/psypanda/hashID) - Identify the different types of hashes used to encrypt data and especially passwords.

[Cryptool](https://www.cryptool.org/en/cryptool-online)

[Cryptoprograms](http://www.cryptoprograms.com/) - Make and decrypt ciphers

[Outguess](https://web.archive.org/web/20150419030527/http://www.outguess.org/)
